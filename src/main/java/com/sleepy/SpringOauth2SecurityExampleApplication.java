package com.sleepy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

@SpringBootApplication
@RestController
@EnableOAuth2Sso
public class SpringOauth2SecurityExampleApplication {

    @GetMapping("/")
    public String home(Principal principal) {
        Map<String, Object> details = (Map<String, Object>) ((OAuth2Authentication) principal).getUserAuthentication().getDetails();
        String uname = (String) details.get("name");
        System.out.println(details);
        return "Hello " + uname + " Welcome to application ><";
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringOauth2SecurityExampleApplication.class, args);
    }

}
